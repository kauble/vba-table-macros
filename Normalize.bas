Sub PullData()
'
' PullData Macro
' Just a wrapper to pass a parameter to the LoadRawRecords procedure.

    ' If something goes wrong, just give up the ghost gracefully
    On Error GoTo BailOutDude

    ' First load the data from the source file
    Call LoadRawRecords("A:P")

    ' After this step, we'll run some reformatting macros & some manual checks

BailOutDude:
End Sub
Sub StandardizeFields()
'
' StandardizeFields Macro
' Sets up fields, extracts color-coded info, & Booleanizes proper fields.

    ' Let's keep the screen from updating first
    Application.ScreenUpdating = False

    ' Let's loop over every worksheet in the book
    For Sheetcounter = 1 To ThisWorkbook.Worksheets.Count

        ' Only resigned records have data in the N-P fields; sort these fields
        ' NB: You have to activate each page explicitly
        Worksheets(Sheetcounter).Activate
        Columns("O:P").Select
        Selection.Cut
        Columns("Q:R").Select
        ActiveSheet.Paste

        ' Find the last record on this sheet
        Lastrow = Cells.Find("*", [A1], , , xlByRows, xlPrevious).Row

        ' Now loop over all records on this sheet and correct them
        For Currentrec = 2 To Lastrow

            ' Extract color codes and booleanize the proper fields
            Call ExtractColors(Currentrec)
            Call BooleanizeChecks(Currentrec)
            Call CleanupFormat(Currentrec)

        Next Currentrec
    Next Sheetcounter

    ' I'm just going to fix the header row in the first sheet for now
    With Sheets(1)
        .Cells(1, "N").Value = "Seeking Certification"
        .Cells(1, "O").Value = "Engineer"
        .Cells(1, "P").Value = "Recommendation Email"
        .Cells(1, "Q").Value = "Reason for Resignation"
        .Cells(1, "R").Value = "Date of Resignation"
    End With

    ' Clear all relevant states
    Application.CutCopyMode = False
    Application.ScreenUpdating = True

    ' After this, it's probably good to fix up as much data as possible ...
    ' Then clear all the formatting

End Sub
Sub AtomizeCourses()
'
' AtomizeCourses Macro
' Splits the comma-separated course lists & gives each course its own record.

    ' More sheet looping...
    For Sheetcounter = 1 To ThisWorkbook.Worksheets.Count

        ' Let's keep the screen from updating first
        Application.ScreenUpdating = False

        ' Excel gets confused and requires explicitly activating a sheet
        Sheets(Sheetcounter).Activate
        ' Make a scratch copy of the courses field & split them
        Dim CourseLists As Worksheet
        Set CourseLists = SplitCourses("I", "B")

        ' We first calculate how many copies to make record copies ...
        CourseLists.Activate
        Lastrow = Cells.Find("*", [A1], , , xlByRows, xlPrevious).Row
        For RecordCount = 2 To Lastrow
            ' Write the number of new records we'll need to create in column A
            Cells(RecordCount, "A").Value = FlexibleCount(RecordCount, "B")
        Next RecordCount

        ' Now the heart of the algorithm begins to beat
        Sheets(Sheetcounter).Activate
        Displacement = 0
        For RecordCount = 2 To Lastrow

            ' We actually want to make sure we're using a fresh record
            Currentrow = RecordCount + Displacement
            ' Read off the number of rows we'll be inserting next ...
            Copynum = CourseLists.Cells(RecordCount, "A").Value
            ' Create the correct number of copies for this record ...
            Call InsertRows(Currentrow, Copynum)

            ' And now the final dash of sorcery... paste in the split courses!
            Call PasteTransposed(Currentrow, "I", CourseLists, RecordCount, _
                "A", "B")

            ' Then finish by updating the displacement
            Displacement = Displacement + Copynum

        Next RecordCount

        ' Now (quietly) delete the scratch sheet to start over
        Application.DisplayAlerts = False
        CourseLists.Delete
        Application.DisplayAlerts = True

        ' Refresh the screen again to verify the algorithm's progress
        Application.ScreenUpdating = True
    Next Sheetcounter

    ' Clear all relevant states at the end
    Application.CutCopyMode = False

End Sub
Private Sub LoadRawRecords(ByVal CopyFields As String)
'
' LoadRawRecords Subprocedure
' Opens the source workbook and copies the raw records to this one.

    ' Let's keep the screen from updating first
    Application.ScreenUpdating = False

    ' First store the current workbook and then open up the tutor list
    Set Scratchbook = ThisWorkbook
    Set Tutorlist = SourceBook()

    ' To ensure everything is copied, we may need to create worksheets
    Sourcecount = Tutorlist.Sheets.Count
    While (Sourcecount > Scratchbook.Sheets.Count)
        Scratchbook.Sheets.Add
    Wend

    ' Now copy all the necessary data to the scratch workbook
    For Counter = 1 To Sourcecount
        With Tutorlist.Worksheets(Counter)

            ' First, explicitly deactivate any filters to be safe
            .AutoFilterMode = False
            ' Next, copy the course data from the source worksheet
            .Columns(CopyFields).Copy

        End With

        With Scratchbook.Worksheets(Counter)

            ' Then activate the scratch workbook and paste in the data
            .Activate
            .Columns(CopyFields).PasteSpecial
            ' Go ahead and copy over the sheet labels too
            .Name = Tutorlist.Worksheets(Counter).Name
            ' Just to be safe re-sort the records to eliminate blank rows
           .Cells.Sort Key1:=Range("A1"), Order1:=xlAscending, Header:=xlYes

        End With

        ' Now display again so we can see this step has completed
        Application.ScreenUpdating = True

    Next Counter

    ' Go ahead and clear the clipboard, then close the source workbook
    Application.CutCopyMode = False
    Tutorlist.Close SaveChanges:=False

End Sub
Private Function SourceBook()
'
' SourceBook Function
' Opens a source workbook invisibly and returns a handle.

    Bookhandle = Application.GetOpenFilename

    If Bookhandle = False Then
        MsgBox "Dude, you didn't pick a file. I'm like so totally bailing.", _
            vbExclamation, "Gnarly Wipeout!"
        Set SourceBook = Nothing
    Else
        Set SourceBook = Workbooks.Open(Bookhandle)
        ActiveWindow.Visible = False
    End If

End Function
Private Sub ExtractColors(ByVal Currentrow As Integer)
'
' ExtractColors Subprocedure
' Processes color-codes within a single record.

    ' I just grabbed the color values & hard-coded them so it's a bit kludgy
    ' Blue = 33, Yellow = 6, Orange = 45

    ' Yellow in the Name fields is used to indicate certification interest
    If Cells(Currentrow, "A").Interior.ColorIndex = 6 Or _
        Cells(Currentrow, "B").Interior.ColorIndex = 6 Then

        Cells(Currentrow, "N").Value = True
    Else
        Cells(Currentrow, "N").Value = False
    End If

    ' Green in the Student ID field indicates a grad student...
    ' but that info is already in the Classification field & redundant

    ' Blue however marks an engineering student so update the proper field
    If Cells(Currentrow, "C").Interior.ColorIndex = 33 Then
        Cells(Currentrow, "O").Value = True
    Else
        Cells(Currentrow, "O").Value = False
    End If

    ' Orange Cells in the next few fields are invalid! Destroy!
    If Cells(Currentrow, "D").Interior.ColorIndex = 45 Then
        Cells(Currentrow, "D").Value = ""
    End If
    If Cells(Currentrow, "E").Interior.ColorIndex = 45 Then
        Cells(Currentrow, "E").Value = ""
    End If
    If Cells(Currentrow, "F").Interior.ColorIndex = 45 Then
        Cells(Currentrow, "F").Value = ""
    End If
    If Cells(Currentrow, "G").Interior.ColorIndex = 45 Then
        Cells(Currentrow, "G").Value = ""
    End If
    If Cells(Currentrow, "H").Interior.ColorIndex = 45 Then
        Cells(Currentrow, "H").Value = ""
    End If

End Sub
Private Sub BooleanizeChecks(ByVal Currentrow As Integer)
'
' BooleanizeChecks Subprocedure
' Converts "x" or bad entries in a cell to a Boolean.

    ' Lets Boolean-ize the Recommendation & GPA fields
    If Cells(Currentrow, "K").Value Like "[xX]" Then
        Cells(Currentrow, "K").Value = True
    ' In case there's other data there, move it to a new field
    Else
        Cells(Currentrow, "P").Value = Cells(Currentrow, "K").Value
        Cells(Currentrow, "K").Value = False
    End If

    If Cells(Currentrow, "L").Value Like "[xX]" Then
        Cells(Currentrow, "L").Value = True
    Else
        Cells(Currentrow, "L").Value = False
    End If

End Sub
Private Sub CleanupFormat(ByVal Currentrow As Integer)
'
' CleanupFormat Subprocedure
' Fixes common text errors in various fields to make the data import smoothly.

    ' Convert names to standard case pattern in columns A & B
    ' Then do consistent pattern for phone numbers in column D
    ' Capitalize major names & classification properly in columns F & G
    ' Convert all T-Shirt sizes to upper case in column H

    ' Text-to-columns can freak out in the presence non-printing characters
    Tmpvalue = Cells(Currentrow, "I").Value
    Cells(Currentrow, "I").Value = WorksheetFunction.Clean(Tmpvalue)

    ' Check for weird capitalization & missing commas in the course lists too
    ' Eventually split up training dates in J...? or do they matter anymore?
    ' Also check lead tutor formatting in column M?
    ' Check resignation dates and reasons (columns Q & R) for proper format

End Sub
Private Function SplitCourses(ByVal DataColumn As String, _
    ByVal ScratchColumn As String)
'
' SplitCourses Subprocedure
' Creates a draft sheet, copies a field to it, then splits lists over commas.

    ' Store the current worksheet & copy the proper column to start
    Set Datahandle = ActiveSheet
    Datahandle.Columns(DataColumn).Select
    Selection.Copy

    ' Now create a temporary sheet for everything
    Set SplitCourses = Sheets.Add(After:=Worksheets(Worksheets.Count))
    With SplitCourses
        .Name = "Scratch"
        .Columns(ScratchColumn).Select
        .Paste
    End With

    ' Then invoke the Text-To-Columns feature
    Selection.TextToColumns DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, Comma:=True, TrailingMinusNumbers:=True

    ' Just to be safe, let's wipe the clipboard & reset state
    Application.CutCopyMode = False
    Datahandle.Activate

End Function
Private Function FlexibleCount(ByVal Currentrow As Integer, _
    ByVal Startcolumn As String)
'
' FlexibleCount Function
' Returns the number of cells at the end of a row, including degenerate rows.

    ' If there is no known course info, handle this case specifically
    If Cells(Currentrow, Startcolumn) Like "" Then
        FlexibleCount = 0
    Else
        ' Hunt down the last entry in this row
        Lastcol = Rows(Currentrow).Find("*", , , , xlByColumns, _
            xlPrevious).Column
        ' Then count the number of cells in this range
        ' NB: At some point, might we want to pass the range itself?
        FlexibleCount = Range(Cells(Currentrow, Startcolumn), _
            Cells(Currentrow, Lastcol)).Count - 1
    End If

End Function
Private Sub InsertRows(ByVal Sourcerow As Integer, ByVal Copynumber As Integer)
'
' InsertRows Subprocedure
' Copies "this" row and inserts it below a given number of times.

    If Copynumber > 0 Then

        ' Grab a handle to the row to be copied and the row below it
        Set Copyrow = Rows(Sourcerow)
        Set Insrow = Rows(Sourcerow + 1)

        ' Then insert the row above the correct number of times
        For Counter = 1 To Copynumber
            Copyrow.Copy
            Insrow.Insert Shift:=xlDown
        Next Counter

    End If
End Sub
Private Sub PasteTransposed(ByVal Pasterow As Integer, _
    ByVal Pastecol As String, ByRef Sourcesheet As Worksheet, _
    ByVal Sourcerow As Integer, ByVal Countcol As String, _
    ByVal Startcol As String)
'
' PasteTransposed Subprocedure
' Transposes and pastes a row of split course lists.

    ' First store the active sheet to load later, then load the draft sheet
    Set Tempsheet = ActiveSheet
    Sourcesheet.Activate

    ' Now use the count in column A of the draft sheet to find a range
    RangeStart = Columns(Startcol).Column
    RangeEnd = RangeStart + Cells(Sourcerow, Countcol).Value

    ' Then select that range, copy it ...
    Range(Cells(Sourcerow, RangeStart), Cells(Sourcerow, RangeEnd)).Select
    Selection.Copy
    ' Switch back to the original sheet ...
    Tempsheet.Activate

    ' Transpose and paste it: Ohhhhh, yeah!
    Cells(Pasterow, Pastecol).PasteSpecial Paste:=xlPasteValues, _
        Transpose:=True

    ' Go ahead and clear the clipboard to be safe
    Application.CutCopyMode = False

End Sub
