Sub GetCourseList()
'
' GetCourseList Macro
' Returns a list of all tutored courses without duplicates.

    ' First load the course list into this workbook to manipulate
    ' TO-DO: Generalize this to handle any range of values,
        ' Also possibly keep formatting at this step
    Call LoadRawCourses

    ' Then split the cells along commas
    Selection.TextToColumns DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, Comma:=True, TrailingMinusNumbers:=True

    ' NB: This might be handy if we want to selectively remove formatting
    ' Selection.ClearFormats

    ' Now cycle over the column and correct the format of the course lists
    ' NB: Currently still under development
'    For Each Coursecell In Selection
'        Call CleanupFormat(Coursecell)
'    Next Coursecell

    ' Now the real magic happens: append all the entries into column A
    ' TO-DO: Generalize this to apply to more granular ranges
    Call Columnize

    ' Finally, consolidate everything to remove duplicates & sort
    Call Consolidate

End Sub
Private Sub LoadRawCourses()
'
' LoadRawCourses Macro
' Opens the source workbook and copies the comma-separated courselist.

    ' Let's keep the screen from updating first
    Application.ScreenUpdating = False

    ' First store the current workbook and then open up the tutor list
    Set Scratchbook = ThisWorkbook
    Set Tutorlist = SourceBook()

    ' NB: Check this, but as of now, courses are in Column I
    ' First copy the course data from the source worksheet
    Tutorlist.Worksheets(1).Columns("I").Copy

    ' Then activate the scratch workbook and paste in the data
    Scratchbook.Worksheets(1).Columns("B").PasteSpecial _
        Paste:=xlPasteValues, Operation:=xlNone

    ' Go ahead and clear the clipboard, then close the source workbook
    Application.CutCopyMode = False
    Tutorlist.Close SaveChanges:=False

    ' Remove the header row so we don't have to worry about it'
    ActiveSheet.Rows(1).Delete Shift:=xlUp

    ' Now display again so we can see this step has completed
    Application.ScreenUpdating = True

End Sub
Private Function SourceBook()

    Bookhandle = Application.GetOpenFilename

    If Bookhandle = False Then
        MsgBox "Dude, you didn't pick a file. I'm like so totally bailing.", _
            vbExclamation, "Gnarly Wipeout!"
        Set SourceBook = Nothing
    Else
        Set SourceBook = Workbooks.Open(Bookhandle)
        ActiveWindow.Visible = False
    End If

End Function
Private Sub Columnize()
'
' Columnize Macro
' Scans all rows, starting from a given column, for values, then transposes,
' moves, and concatenates them in another column.

    ' Turn off screen updating to improve performance
    Application.ScreenUpdating = False
    On Error Resume Next

    ' First store the data column
    Colindex = Selection.Column

    ' Initialize the paste counter and find the last row to scan
    Pastetop = 2
    Lastrow = Cells.Find("*", [A1], , , xlByRows, xlPrevious).Row

    For i = 1 To Lastrow
        Set Startcell = Cells(i, Colindex)

        ' Check that row is nonempty first
        If (Startcell <> "") Then

            ' Scan the row for the last value
            Lastcol = Startcell.EntireRow.Find("*", , , , xlByColumns, _
                xlPrevious).Column

            ' Select and count relevant cells, then determine target range
            Set Relevant = Range(Cells(i, Colindex), Cells(i, Lastcol))
            Recordsize = Relevant.Count
            Pastebottom = Pastetop + Recordsize - 1

            ' Copy and move relevant cells, then advance the paste counter
            Relevant.Copy
            Range(Cells(Pastetop, "A"), Cells(Pastebottom, "A")).PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, Transpose:=True
            Pastetop = Pastebottom + 1
            Relevant.ClearContents
        End If
    Next i

    ' Flush the error buffer and update the display
    Err.Clear
    Application.ScreenUpdating = True

End Sub
Private Sub Consolidate()
' Consolidate Macro
' Removes duplicates and sorts all course numbers.

    Columns("A").RemoveDuplicates Columns:=1
    Columns("A").Sort Key1:=Range("A1"), Order1:=xlAscending

End Sub
