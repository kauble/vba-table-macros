Sub ReformatSheet()
'
' ReformatSheet macro
' This macro just wraps the CorrectPatterns subprocedure and applies it to all
' records in the sheet.

    Lastrow = Cells.Find("*", [A1], , , xlByRows, xlPrevious).Row
    For i = 1 To Lastrow
        Call CorrectPatterns(i)
    Next i

End Sub
Private Sub CorrectPatterns(ByVal Currentrow As Integer)
'
' CorrectPatterns Macro
' Fix errors common to each field and mark unusual ones to check manually.

    ' NB: This macro requires the Microsoft Regular Expressions reference

    ' For name fields, we just check for the usual format, & mark exceptions
    Set OneName = New VBScript_RegExp_55.RegExp
    OneName.Pattern = "^(\w)(\w+)$"

    With Cells(Currentrow, "A")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not OneName.Test(tmpstring) Then
            .Interior.ColorIndex = 45
            .Value = tmpstring
        Else
            .Interior.ColorIndex = 0

            Set Tmpmatch = OneName.Execute(tmpstring)
            Capital = UCase(Tmpmatch(0).SubMatches(0))
            Tail = LCase(Tmpmatch(0).SubMatches(1))

            .Value = Capital & Tail

        End If
    End With

    With Cells(Currentrow, "B")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not OneName.Test(tmpstring) Then
            .Interior.ColorIndex = 45

            .Value = tmpstring
        Else
            .Interior.ColorIndex = 0

            Set Tmpmatch = OneName.Execute(tmpstring)
            Capital = UCase(Tmpmatch(0).SubMatches(0))
            Tail = LCase(Tmpmatch(0).SubMatches(1))

            .Value = Capital & Tail

        End If
    End With

    ' For the student IDs, we can only check for obvious errors
    Set IDNumber = New VBScript_RegExp_55.RegExp
    IDNumber.Pattern = "^\d{8}$"

    With Cells(Currentrow, "C")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not IDNumber.Test(tmpstring) Then
            .Interior.ColorIndex = 45
        Else
            .Interior.ColorIndex = 0
        End If

        .Value = tmpstring
    End With

    ' For the phone numbers, we check for 9 digits & hyphenate
    Set PhoneNum = New VBScript_RegExp_55.RegExp
    PhoneNum.Pattern = "^\D*(\d{3})\D*(\d{3})\D*(\d{4})\D*$"

    With Cells(Currentrow, "D")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not PhoneNum.Test(tmpstring) Then
            .Interior.ColorIndex = 45

            .Value = tmpstring
        Else
            .Interior.ColorIndex = 0

            Set Tmpmatch = PhoneNum.Execute(tmpstring)
            AreaCode = Tmpmatch(0).SubMatches(0)
            NumPrefix = Tmpmatch(0).SubMatches(1)
            NumPostfix = Tmpmatch(0).SubMatches(2)

            .Value = AreaCode & "-" & NumPrefix & "-" & NumPostfix

        End If
    End With

    ' For the emails, we only check for a proper format
    Set Email = New VBScript_RegExp_55.RegExp
    Email.Pattern = "^\w+(\.\w+)*@(\w+\.)+\w+$"

    With Cells(Currentrow, "E")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not Email.Test(tmpstring) Then
            .Interior.ColorIndex = 45
        Else
            .Interior.ColorIndex = 0
        End If

        .Value = tmpstring
    End With

    ' Eventually, we could scan for certain patterns for majors
    ' Also use a switch statement to check for allowed classifications

    ' Ditto for shirt sizes, although we can standardize case too
    Set ShirtSize = New VBScript_RegExp_55.RegExp
    ShirtSize.Pattern = "^[xX]{0,2}[sSmMlL]$"

    With Cells(Currentrow, "H")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not ShirtSize.Test(tmpstring) Then
            .Interior.ColorIndex = 45

            .Value = tmpstring
        Else
            .Interior.ColorIndex = 0

            Set Tmpmatch = ShirtSize.Execute(tmpstring)
            .Value = UCase(Tmpmatch(0))

        End If
    End With

    ' Now the really important part, correcting the course codes
    ' It just marks the grad student's courses though
    Set TypicalCourse = New VBScript_RegExp_55.RegExp
    Set PunctTrim = New VBScript_RegExp_55.RegExp

    ' Use reg-exps both for normal courses & chopping out punctuation
    TypicalCourse.Pattern = "\W*([A-Za-z]{3,4})\W*(\d{4})\W*"
    PunctTrim.Pattern = "^\W*(\w|\w.*\w)\W*$"
    TypicalCourse.Global = True

    With Cells(Currentrow, "I")

        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        ' We use remstring to keep track of atypical entries in the list
        remstring = tmpstring

        If TypicalCourse.Test(tmpstring) Then
            Set Courselist = TypicalCourse.Execute(tmpstring)

            ' For every typical course found ...
            For Each messycourse In Courselist

                ' Standardize its format ...
                coursedept = UCase(messycourse.SubMatches(0))
                coursenum = messycourse.SubMatches(1)
                cleancourse = coursedept & " " & coursenum

                ' Then separate it in the draft string ...
                tmpstring = Replace(tmpstring, messycourse, _
                                    "," & cleancourse & ",")
                ' And remove it from the remainder string
                remstring = Replace(remstring, messycourse, ",")

            Next
        End If

        ' Now chop extraneous punctuation from the draft string
        tmpstring = PunctTrim.Replace(tmpstring, "$1")
        tmpstring = Replace(tmpstring, ",,", ",")

        ' Flag the cell if the remainder string still has word characters
        If PunctTrim.Test(remstring) Then
            .Interior.ColorIndex = 45
        Else
            .Interior.ColorIndex = 0
        End If

        .Value = tmpstring
    End With

    ' Finally let's check various dates of the form "Season YYYY"
    Set SeasonYear = New VBScript_RegExp_55.RegExp
    SeasonYear.Pattern = "^([sS][pP]|[sS][uU]|[fF]|[wW])\w*\s*\d*(\d{2})$"

    With Cells(Currentrow, "R")
        tmpstring = .Value
        tmpstring = WorksheetFunction.Clean(tmpstring)
        tmpstring = WorksheetFunction.Trim(tmpstring)

        If Not SeasonYear.Test(tmpstring) Then
            .Interior.ColorIndex = 45
            .Value = tmpstring
        Else
            Set Parsedate = SeasonYear.Execute(tmpstring)
            Seasonpre = Parsedate(0).SubMatches(0)
                        ' I think we can stay in this century for now
            YYYY = "20" & Parsedate(0).SubMatches(1)

            ' Write out the complete season, even given minimal abbreviations
            Select Case Seasonpre
                Case "sp", "Sp", "sP", "SP"
                    Season = "Spring"
                Case "su", "Su", "sU", "SU"
                    Season = "Summer"
                Case "f", "F"
                    Season = "Fall"
                Case "w", "W"
                    Season = "Winter"
            End Select

            ' And finish by concatenating the season and year
            .Value = Season & " " & YYYY
            .Interior.ColorIndex = 0

        End If
    End With
End Sub
